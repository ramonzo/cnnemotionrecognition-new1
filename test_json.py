import codecs
import json
import numpy as np

obj_text = codecs.open('data/x_train_VGG_output.json', 'r', encoding='utf-8').read()
b_new = json.loads(obj_text)
x_train_feature_map = np.array(b_new)
print(x_train_feature_map.shape)